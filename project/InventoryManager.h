#pragma once
#include "Exam_HelperStructs.h"
class IExamInterface;
class BotMemory;

class InventoryManager
{
public:
	InventoryManager(IExamInterface* Interface, BotMemory* Memory);
	~InventoryManager();
	void NewFrame();

	void FoundItem(EntityInfo Item);
	void Consume(UINT slot);
	void FillSlot(UINT slot, ItemInfo item);
	void RemoveSlot(UINT slot);


	void FoundGun(ItemInfo FoundItem);
	void FoundMedKit(ItemInfo FoundItem);
	void FoundFood(ItemInfo FoundItem);

	bool HasEnoughDamageToKill(int enemyHp);
	bool ShootEnemy(EnemyInfo Enemy);


	void TookDamage(float damage);
	void LostEnergy(float energyLoss);

	int GetEmptySlots();

	bool m_CanUseThisFrame=true;
	bool m_slotsFilled[5]={false,false, false, false, false};

	IExamInterface* m_pInterface;
	BotMemory* m_pBotMemory;
};

