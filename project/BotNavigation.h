#pragma once
#include "Exam_HelperStructs.h"

class IExamInterface;

class BotNavigation
{
public:
	BotNavigation(IExamInterface* Interface);
	~BotNavigation();

	void NewFrame(float dt);

	void UpdateAngularVelocity(SteeringPlugin_Output& steering);
	void SetRunMode(SteeringPlugin_Output& steering, AgentInfo agent, bool hasPanic);
	void LookAt(SteeringPlugin_Output& steering, AgentInfo agent, Elite::Vector2 target);


	IExamInterface* m_pInterface;
	bool m_StaminaRun=false;
	bool m_PanicRecovery=false;

	bool m_LookingAtTarget=false;
	float m_TargetAngle;
};

