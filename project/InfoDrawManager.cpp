#include "stdafx.h"
#include "InfoDrawManager.h"
#include "Exam_HelperStructs.h"

#include "IExamInterface.h"
#include "ExplorationGrid.h"
#include "BotMemory.h"

InfoDrawManager::InfoDrawManager(IExamInterface* Interface, ExplorationGrid* NavGrid, BotMemory* Memory):m_pInterface(Interface),m_pNavGrid(NavGrid) ,m_pBotMemory(Memory)
{
}


InfoDrawManager::~InfoDrawManager()
{
}

void InfoDrawManager::Draw(AgentInfo Agent, Elite::Vector2 Target,float totalRuntime)
{
	if (!hasWorldInfo)
	{
		hasWorldInfo = true;
		WorldDimensions= m_pInterface->World_GetInfo().Dimensions/2;
	}

	//m_pInterface->Draw_SolidCircle(Target, .7f, { 0,0 }, { 1, 0, 0 });
	//m_pInterface->Draw_SolidCircle(m_pInterface->NavMesh_GetClosestPathPoint(Target), .7f, { 0,0 }, { 1, 0, 0 });

	for (auto enemy : m_EnemiesToDraw)
	{
		//m_pInterface->Draw_SolidCircle(enemy.Location, .8f, { 0,0 }, { 1, 0, 0 }, -0.75f);
		DrawNumber(enemy.Location+ Elite::Vector2{0,0.5f},enemy.Health,{1,0.9f,0.9f});
	}
	m_EnemiesToDraw.clear();


	/*seen items*/
	//#define DRAW_ITEM_MEMORY
	#ifdef DRAW_ITEM_MEMORY
		for (auto Item : m_pBotMemory->m_SeenItems)
		{
			m_pInterface->Draw_SolidCircle(Item.Location, .5f, { 0,0 }, { 0, 1, 0 },-0.75f);
		}
	#endif

	/*LOOT POINTS*/
	//#define DRAW_LOOT_POINTS
	#ifdef DRAW_LOOT_POINTS
		for (auto point : m_pNavGrid->m_LootPoints)
		{
			m_pInterface->Draw_SolidCircle(point, .5f, { 0,0 }, { 1,1,1}, -0.85f);
		}
	#endif
	
	/*Memorized Houses*/
//	#define DRAW_HOUSE_MEMORY
	#ifdef DRAW_HOUSE_MEMORY
	for (auto House : m_pBotMemory->m_KnownHouses)
	{

		#define HOUSETIMESINCELOOT
		
		#define HOUSEBOX
		//#define HOUSECROSS

		#ifdef HOUSEBOX
			Elite::Vector2 verts[4]
			{
				Elite::Vector2{ House->baseInfo.Center.x - House->baseInfo.Size.x / 2   ,House->baseInfo.Center.y - House->baseInfo.Size.y / 2 },
				Elite::Vector2{ House->baseInfo.Center.x + House->baseInfo.Size.x / 2 ,House->baseInfo.Center.y - House->baseInfo.Size.y / 2 },
				Elite::Vector2{ House->baseInfo.Center.x + House->baseInfo.Size.x / 2   ,House->baseInfo.Center.y + House->baseInfo.Size.y / 2 },
				Elite::Vector2{ House->baseInfo.Center.x - House->baseInfo.Size.x / 2 ,House->baseInfo.Center.y + House->baseInfo.Size.y / 2 }
			};
			m_pInterface->Draw_Polygon(&verts[0], 4, {1,0,0},-0.80f);
		#endif

		#ifdef HOUSECROSS
		m_pInterface->Draw_Segment({ House->baseInfo.Center.x - House->baseInfo.Size.x / 2   ,House->baseInfo.Center.y - House->baseInfo.Size.y / 2 }, { House->baseInfo.Center.x + House->baseInfo.Size.x / 2   ,House->baseInfo.Center.y + House->baseInfo.Size.y / 2 },{0,1,0},-0.8f);
		m_pInterface->Draw_Segment({ House->baseInfo.Center.x - House->baseInfo.Size.x / 2 ,House->baseInfo.Center.y + House->baseInfo.Size.y / 2 }, { House->baseInfo.Center.x + House->baseInfo.Size.x / 2 ,House->baseInfo.Center.y - House->baseInfo.Size.y / 2 }, { 0,1,0 }, -0.8f);	
		#endif


		#ifdef HOUSETIMESINCELOOT
		DrawNumber(House->baseInfo.Center,static_cast<int>((totalRuntime- House->m_LastLootTime)));
		#endif

	}
	#endif


	//#define CURRENTCELL
	//#define FULLGRID
	
	//#define EXPLOREDGRID
	//#define UNEXPLORED
	

	#ifdef FULLGRID
	for (auto pCell:m_pNavGrid->m_vWorldCells)
	{
		DrawCell(pCell);
	}
	#endif

	#ifdef EXPLOREDGRID
	for (auto pCell : m_pNavGrid->m_vWorldCells)
	{
		if(pCell->explored)
			DrawCell(pCell,{0,0,0.5f});
		#ifdef UNEXPLORED
		else
			DrawCell(pCell, { 0.5f,0,0 });
		#endif
	}
	#endif


	#ifdef CURRENTCELL
	auto cell =m_pNavGrid->getCell(Agent.Position);
	DrawCell(cell,{1,0,0});
	Elite::Vector2 verts[4]
	{
		Elite::Vector2{ cell->center.x - cell->size / 2 ,cell->center.y - cell->size / 2 },
		Elite::Vector2{ cell->center.x + cell->size / 2,cell->center.y - cell->size / 2 },
		Elite::Vector2{ cell->center.x + cell->size / 2,cell->center.y + cell->size / 2 },
		Elite::Vector2{ cell->center.x - cell->size / 2,cell->center.y + cell->size / 2 }
	};
	m_pInterface->Draw_Polygon(&verts[0], 4, {1,1,1}, -0.75f);
	#endif
}

void InfoDrawManager::DrawCell(NavCell* cell, Elite::Vector3 color)
{
	#define BOX
	#define CENTER

	#ifdef BOX
	Elite::Vector2 verts[4]
	{
		Elite::Vector2{ cell->center.x - cell->size / 2   ,cell->center.y - cell->size / 2 },
		Elite::Vector2{ cell->center.x + cell->size / 2 ,cell->center.y - cell->size / 2 },
		Elite::Vector2{ cell->center.x + cell->size / 2 ,cell->center.y + cell->size / 2 },
		Elite::Vector2{ cell->center.x - cell->size / 2 ,cell->center.y + cell->size / 2 }
	};
	m_pInterface->Draw_Polygon(&verts[0], 4, color, -0.75f);
	#endif

	#ifdef CENTER
	m_pInterface->Draw_Circle(cell->center,0.5f,color);
	#endif

}


void InfoDrawManager::DrawNumber(Elite::Vector2 origin, int Number,Elite::Vector3 color)
{
	const int blockSize = 5;
	const Elite::Vector2 offset = { 0.4f,0.5f };

	int numberOfBlocks = static_cast<int>(Number/ blockSize);
	int numberOfTurfs = Number%blockSize;
	

	for (int i = 0; i < numberOfBlocks; ++i)
	{
		Elite::Vector2 verts[4]
		{
			Elite::Vector2{ origin.x+ offset.x + i* blockSize*offset.x,origin.y-offset.y},
			Elite::Vector2{ origin.x + (i+1)* blockSize*offset.x,origin.y - offset.y },
			Elite::Vector2{ origin.x + (i + 1)* blockSize*offset.x,origin.y + offset.y },
			Elite::Vector2{ origin.x + offset.x + i* blockSize*offset.x,origin.y + offset.y }
		};
		m_pInterface->Draw_SolidPolygon(&verts[0], 4, color, -0.75f);
	}

	for (int i = 1+blockSize*numberOfBlocks; i <= Number; ++i)
	{
		m_pInterface->Draw_Segment(
			Elite::Vector2{ origin.x + i*offset.x ,origin.y +offset.y},
			Elite::Vector2{ origin.x + i*offset.x ,origin.y -offset.y },
			color,-0.75f);
	}
}

