#pragma once
#include "Exam_HelperStructs.h"
#include "CustomStructs.h"
#include <map>

class InventoryManager;
class BotNavigation;
class ExplorationGrid;
class BotMemory;
class IExamInterface;

enum DangerState
{
	CALM,
	FIGHT,
	PANIC
};

enum MovementGoals
{
	EXPLORE,
	FINDGOAL,
	ENTERHOUSE,
	LOOT,
	SCAVENGE,
	REST
};

class UtilityManager
{
public:
	UtilityManager(IExamInterface* Interface,InventoryManager* Inventory, BotNavigation* Navigation, ExplorationGrid* ExplorationGrid, BotMemory* Memory);
	~UtilityManager();
	bool FireAtEnemy(SteeringPlugin_Output& steering, AgentInfo agent, EnemyData target);

	void UpdateState(float dt);
	void UpdateUtility(float dt, SteeringPlugin_Output& steering);


	DangerState m_CurrentDangerState=CALM;
	MovementGoals m_CurrentMovementGoal = EXPLORE;
	map<MovementGoals,float> m_MovementUtility ={};

	bool m_InsideHouseLastFrame=false;
	bool m_EnteredThisFrame = false;

	bool m_WasBit=false;

	float m_RunTime=0;

	//Don't get stuck
	Elite::Vector2 m_LastPosition;
	const float m_StuckArea=10.f;
	const float m_PatienceTreshold = 5.f;
	const float m_PatienceTreshold2 = 10.f;
	const float m_PatienceTreshold3 = 20.f;
	float m_PatienceTimer=0;


	IExamInterface* m_pInterface;
	//Managers to get info from
	InventoryManager* m_pInventoryManager;
	BotNavigation* m_pNavigation;
	ExplorationGrid* m_pNavGrid;
	BotMemory* m_pBotMemory;

};

