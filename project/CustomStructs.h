#pragma once
#include "IExamInterface.h"
#include <vector>
#include "EliteMath/EVector2.h"

class ExplorationGrid;

struct NavCell
{
	const float size;
	const int mainIndex;

	Elite::Vector2 center;
	bool explored = false;

	NavCell(int Index, float Size, Elite::Vector2 Center) :mainIndex(Index), size(Size), center(Center) {}

	float distance(Elite::Vector2 target)
	{
		return Elite::Distance(center, target);
	}
};


// Static/global constants
static float STAMINATRESHOLD=8.0f;

class HouseData
{
public:
	HouseData(HouseInfo info, ExplorationGrid* navGrid);
	//~HouseData();
	vector<Elite::Vector2> GetLootPoints();
	float DistanceTo(Elite::Vector2 target);
	float LootCD(float runTime);

	//********
	operator HouseInfo() const { return baseInfo; }

	//********
	const HouseInfo baseInfo;
	std::vector<NavCell*> m_vHouseCells;
	float m_LastLootTime;

};

class EnemyData
{
public:
	EnemyData(EnemyInfo info, bool canKill):m_BaseInfo(info), m_CanKill(canKill){};
	operator EnemyInfo() const { return m_BaseInfo; }
	//********
	const EnemyInfo m_BaseInfo;
	const bool m_CanKill=false;
};
