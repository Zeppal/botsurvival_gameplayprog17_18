//Helper Functions
#include "stdafx.h"
#include "HelperFunctions.h"
#include "IExamInterface.h"

Elite::Vector2 Helper::getRotationToPoint(const float rotation, Elite::Vector2 startRotation)
{
	float s = sin(rotation);
	float c = cos(rotation);

	return Elite::Vector2{
		startRotation.x * c - startRotation.y * s,
		startRotation.x * s + startRotation.y * c
	};
}


float Helper::getEnemyAngle(Elite::Vector2 agentPos, Elite::Vector2 enemyPos, const float baseRotation)
{
	enemyPos -= agentPos;
	enemyPos.Normalize();

	float s = sin(baseRotation);
	float c = cos(baseRotation);

	return   fmod(atan2(enemyPos.x * s + enemyPos.y * c, enemyPos.x * c - enemyPos.y * s), 6.28318548f);
}



vector<HouseInfo> Helper::GetHousesInFOV(IExamInterface* Interface)
{
	vector<HouseInfo> vHousesInFOV = {};

	HouseInfo hi = {};
	for (int i = 0;; ++i)
	{
		if (Interface->Fov_GetHouseByIndex(i, hi))
		{
			vHousesInFOV.push_back(hi);
			continue;
		}

		break;
	}

	return vHousesInFOV;
}

vector<EntityInfo> Helper::GetEntitiesInFOV(IExamInterface* Interface)
{
	vector<EntityInfo> vEntitiesInFOV = {};

	EntityInfo ei = {};
	for (int i = 0;; ++i)
	{
		if (Interface->Fov_GetEntityByIndex(i, ei))
		{
			vEntitiesInFOV.push_back(ei);
			continue;
		}

		break;
	}

	return vEntitiesInFOV;
}

bool Helper::PointInHouse(HouseInfo house,Elite::Vector2 point)
{
	return (point.x > house.Center.x - house.Size.x / 2 &&		
		point.x < house.Center.x + house.Size.x / 2 &&
		point.y > house.Center.y - house.Size.y / 2 &&
		point.y < house.Center.y + house.Size.y / 2);
}
