#include "stdafx.h"
#include "UtilityManager.h"
#include "InventoryManager.h"
#include "BotNavigation.h"
#include "ExplorationGrid.h"
#include "BotMemory.h"
#include "HelperFunctions.h"


UtilityManager::UtilityManager(IExamInterface* Interface, InventoryManager* Inventory, BotNavigation* Navigation, ExplorationGrid* ExplorationGrid, BotMemory* Memory):
	m_pInterface(Interface), m_pInventoryManager(Inventory),m_pNavigation(Navigation),m_pNavGrid(ExplorationGrid),m_pBotMemory(Memory),m_RunTime(0.f)
{
}

UtilityManager::~UtilityManager()
{
}


bool UtilityManager::FireAtEnemy(SteeringPlugin_Output& steering, AgentInfo agent, EnemyData target)
{
	EnemyInfo eInfo = static_cast<EnemyInfo>(target);

	m_pNavigation->LookAt(steering, agent, eInfo.Location);

	//Only fire if you can hit
	auto enemyAngle = Helper::getEnemyAngle(agent.Position, eInfo.Location);
	auto angleDiff = fmod(agent.Orientation, 3.15f) - enemyAngle;
	if (angleDiff<0.03f&&angleDiff>-0.03f)
	{
		const bool result = (target.m_CanKill && m_pInventoryManager->ShootEnemy(eInfo));
		return result;
	}

	return false;
}


float LIMIT(float ValueToLimit,float min,float max)
{
	if(ValueToLimit<min)
		return min;
	if(ValueToLimit>max)
		return max;

	return ValueToLimit;
}


void UtilityManager::UpdateState(float dt)
{
	//************
	//Mental state
	if(m_pBotMemory->m_VisibleEnemies.size()>0 || m_WasBit)
	{	//Danger

		if(m_pBotMemory->GetKillableEnemies().size()>0)
		{
			m_CurrentDangerState=FIGHT;
		}
		else
		{
			m_CurrentDangerState = PANIC;
		}	
	}
	else if( m_CurrentDangerState==PANIC)
	{
		//Once you hit a panic, only calm down after getting enough energy to run back
		if(m_pInterface->Agent_GetInfo().Stamina >= STAMINATRESHOLD)
		{
			m_CurrentDangerState=CALM;
		}
	}
	else //No enemies in sight, and not currently panicking
	{
		m_CurrentDangerState=CALM;
	}


	//******************
	// Utility action descision
	//******************
	const auto agent = m_pInterface->Agent_GetInfo();
	const auto world = m_pInterface->World_GetInfo();
	const float HalfWorldDistance=Elite::Distance({0,0},(world.Dimensions/2));
	const auto checkpoint = m_pInterface->World_GetCheckpointLocation();

	if(m_InsideHouseLastFrame==false && agent.IsInHouse)
	{
		m_EnteredThisFrame=true;
	}


	//**************
	//EXPLORE (+0 | +80)
	{
		float utility=-1.f;
		if (m_CurrentDangerState == CALM)
			if(m_pNavGrid->UnexploredCells()>0)//no Unexplored tiles == -1 => never execute
				if(m_pInventoryManager->GetEmptySlots()==0)
					if(Elite::Distance(m_pNavGrid->getClosestUnkownCell(agent.Position)->center, agent.Position)<Elite::Distance(agent.Position, checkpoint))
						utility=70.0f;
		m_MovementUtility[EXPLORE] = utility;
	}

	//**************
	//FINDGOAL
	{
		float utility = 0;
			//Goal in unknown house -> adds house to map and likely to contain items (+0 | +80)
		if(m_pBotMemory->GetHouseFromPoint(checkpoint)==nullptr)
			utility+=20.f;
	
			//Close to goal (-20/+80)
		utility+=  100.f* (0.8f - LIMIT(Elite::Distance(agent.Position,checkpoint)/HalfWorldDistance,0,1));//Lose interest the further away it is

			//Low priority when low on items (-20 | +0)
		if(m_pInventoryManager->GetEmptySlots()>1)
			utility -=20.f;

		if (m_pBotMemory->GetHouseFromPoint(checkpoint) == nullptr &&m_pInventoryManager->GetEmptySlots()>=2)//In danger AND unknown houses left
			utility += 40.f;

		m_MovementUtility[FINDGOAL] = utility;
	}


	//**************
	//ENTERHOUSE (+0 | +10 | +30 | [+60] (+70|+100))
	{
		float utility = 0;
		if(!agent.IsInHouse) //No need to look for the next house while inside one
		{
			if(m_CurrentDangerState==PANIC)
			{
				utility=99.f;
			}
			else
			{
				if (m_pBotMemory->GetUncheckedHouses().size()>0)
				{
					utility += 60.f;
				}
				if (m_pInventoryManager->GetEmptySlots()>1)
					utility += 10.f;
				if (m_pInventoryManager->GetEmptySlots()>3)
					utility += 30.f;				
			}				
		}
		m_MovementUtility[ENTERHOUSE] = utility;
	}

	//**************
	//LOOT
	{
		float utility = 0;
		if(m_pNavGrid->m_LootPoints.size()>0)
		{
			utility = 50.f;
		}
		m_MovementUtility[LOOT] = utility;
	}


	//**************
	//SCAVENGE
	{
		float utility = 0;

		if (m_pBotMemory->m_SeenItems.size()>0)
		{
			utility = 75.f;
			if(m_pInventoryManager->GetEmptySlots()>1)
				utility += 60.f;
		}
		m_MovementUtility[SCAVENGE] = utility;
	}


	//**************
	//REST
	{
		float utility = 0;
		if(agent.IsInHouse && m_CurrentDangerState!=PANIC && (agent.Stamina<STAMINATRESHOLD))
		{
			utility=55.f;//Higher then loot priority, lower then getting items or a close goal
		}
		m_MovementUtility[REST] = utility;
	}


	//**************
	//Pick most usefull behavior
	auto it = std::max_element
			(
				m_MovementUtility.begin(), m_MovementUtility.end(),
				[](const pair<MovementGoals, float>& p1, const pair<MovementGoals, float>& p2)
					{
						return p1.second < p2.second;
					}
			);
	m_CurrentMovementGoal = (*it).first;

}

void UtilityManager::UpdateUtility(float dt, SteeringPlugin_Output& steering)
{
	m_RunTime+=dt;
	m_EnteredThisFrame=false;
	UpdateState(dt);


	//Movement goal
	Elite::Vector2 m_Target;
	vector<Elite::Vector2> m_DirectionToBlend;

	//***************
	//Update steering
	auto agentInfo = m_pInterface->Agent_GetInfo();

	/*Fight or flight behavior rotation*/
	{
		if(m_CurrentDangerState==FIGHT)
		{
			for (auto enemy : m_pBotMemory->GetKillableEnemies())
			{
				FireAtEnemy(steering, agentInfo, enemy);
			}
		}
		else if(m_CurrentDangerState == PANIC && m_pBotMemory->m_VisibleEnemies.size()>0)
		{
			for (auto enemy : m_pBotMemory->m_VisibleEnemies)
			{
				m_pNavigation->LookAt(steering,agentInfo, enemy.m_BaseInfo.Location);
				Elite::Vector2 direction = enemy.m_BaseInfo.Location - agentInfo.Position; 
				direction *=-0.5f;
				m_DirectionToBlend.push_back(direction);
			}
		}
	}



	//***************
	//Get the goal/target
	switch (m_CurrentMovementGoal) { 
		case EXPLORE: 
			m_Target = m_pNavGrid->getClosestUnkownCell(agentInfo.Position)->center;
		break;

		case FINDGOAL:
			m_Target = m_pInterface->World_GetCheckpointLocation();
		break;

		case ENTERHOUSE: 
			if(m_pBotMemory->m_KnownHouses.size()>0)
			{
				HouseData* targetHouse;
				if(m_CurrentDangerState==PANIC)
					targetHouse = m_pBotMemory->GetClosestHouse(agentInfo.Position,0,m_RunTime);
				else
					targetHouse = m_pBotMemory->GetClosestHouse(agentInfo.Position, 1000,m_RunTime);

				m_Target = targetHouse->baseInfo.Center;
			}
			else
				m_Target = m_pInterface->World_GetCheckpointLocation(); //Fallback target = goal

		break;


		case LOOT:
			if (m_pNavGrid->m_LootPoints.size()>0)
				m_Target = m_pNavGrid->m_LootPoints[0]; 
			else
				m_Target = m_pInterface->World_GetCheckpointLocation(); //Fallback target = goal
		break;
		case SCAVENGE:
			if (m_pBotMemory->m_SeenItems.size()>0)
				m_Target = m_pBotMemory->m_SeenItems[0].Location;
			else
				m_Target = m_pInterface->World_GetCheckpointLocation(); //Fallback target = goal
		break;
		case REST:
			{
				const auto currentHouse = m_pBotMemory->GetHouseFromPoint(agentInfo.Position);
				if(currentHouse!= nullptr)
					m_Target= currentHouse->baseInfo.Center;
				else
					m_Target = m_pInterface->World_GetCheckpointLocation(); //Fallback target = goal
			}
		break;
		default: ;
	}


	//***************
	//Setup or destroy the helper waypoints
	if(agentInfo.IsInHouse)
	{
		const auto currentHouse = m_pBotMemory->GetHouseFromPoint(agentInfo.Position);
		if(currentHouse!=nullptr)
		{
			if(m_EnteredThisFrame && (currentHouse->LootCD(m_RunTime)>10))
				m_pNavGrid->LootHouseCells(*currentHouse);
	
			currentHouse->m_LastLootTime=m_RunTime;
		}
	}
	else if(!agentInfo.IsInHouse && m_pNavGrid->m_LootPoints.size()>0)
		m_pNavGrid->m_LootPoints.clear();


	//Desired Velocity	 
	auto dist = Elite::Distance(agentInfo.Position, m_Target);
	if(Elite::Distance(agentInfo.Position,m_Target)<0.2f)
	{
		Elite::Vector2 dir  = m_pInterface->NavMesh_GetClosestPathPoint(m_Target) - agentInfo.Position;
		m_DirectionToBlend.push_back(dir*-1.f);
	}else
	{
		m_DirectionToBlend.push_back( m_pInterface->NavMesh_GetClosestPathPoint(m_Target) - agentInfo.Position);
	}


	/*Anti-sticky corners*/
	if(m_PatienceTimer<=0)
	{
		m_LastPosition = agentInfo.Position;
		m_PatienceTimer += dt;
	}
	else
	{
		m_PatienceTimer += dt;
		auto testDist = Elite::Distance(m_LastPosition, agentInfo.Position);
		if (Elite::Distance(m_LastPosition, agentInfo.Position) <= m_StuckArea)
		{
			if (m_PatienceTimer>m_PatienceTreshold3)
			{
				m_DirectionToBlend.push_back(Elite::Vector2{0,0} - agentInfo.Position);//GOAL is the issue
			}
			else if (m_PatienceTimer>m_PatienceTreshold2)
			{
				m_DirectionToBlend.push_back(m_pInterface->World_GetCheckpointLocation() - agentInfo.Position);//Head directly in the direction of the goal (pathPoint, clearly broke at this point)
			}
			else if (m_PatienceTimer>m_PatienceTreshold)
			{
				m_DirectionToBlend.push_back(m_pInterface->NavMesh_GetClosestPathPoint({0,0}) - agentInfo.Position);//Head towards a pathPoint for the goal			
			}
		}
		else
		{
			m_PatienceTimer = 0;
		}		
	}
		


	//Blend directions
	for(auto direction:m_DirectionToBlend)
	{
		steering.LinearVelocity+=direction;
	}
	
	steering.LinearVelocity.Normalize(); //Normalize Desired Velocity
	steering.LinearVelocity *= agentInfo.MaxLinearSpeed; //Rescale to Max Speed


	//*******************
	m_pNavigation->UpdateAngularVelocity(steering);
	m_pNavigation->SetRunMode(steering, agentInfo,(m_CurrentDangerState==PANIC)); //See if you want to run

	m_WasBit=false;
	m_InsideHouseLastFrame = agentInfo.IsInHouse;
}
