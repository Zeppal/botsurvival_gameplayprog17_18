#include "stdafx.h"
#include "BotMemory.h"
#include "ExplorationGrid.h"
#include "HelperFunctions.h"

//Requires navgrid to setup houseData
BotMemory::BotMemory(ExplorationGrid* explorationGrid):m_pNavGrid(explorationGrid)
{
}

BotMemory::~BotMemory()
{
	for(auto house:m_KnownHouses)
	{
		SAFE_DELETE(house);
	}
	m_KnownHouses.clear();
}

void BotMemory::NextFrame()
{
	m_VisibleEnemies.clear();
}

HouseData* BotMemory::GetHouseFromPoint(Elite::Vector2 targetPoint)
{
	for(auto houseData:m_KnownHouses)
	{
		if (Helper::PointInHouse(houseData->baseInfo, targetPoint))
			return houseData;
	}
	return nullptr;
}

vector<HouseData*> BotMemory::GetUncheckedHouses()
{
	vector<HouseData*> r={};
	if(m_NewHouses>0){
		for (auto houseData : m_KnownHouses)
		{
			if (houseData->m_LastLootTime==0)
				r.push_back(houseData);
		}
		if(r.size()==0)
			m_NewHouses=0;
	}
	return r;
}

HouseData* BotMemory::GetClosestHouse(Elite::Vector2 targetPoint, float lootTimerTreshold,float currentRuntime)
{
	if(m_KnownHouses.size()<=0)
		return nullptr;

	HouseData* ClosestHouse = nullptr;
	HouseData* OldestLootTimer= m_KnownHouses[0];

	for (auto houseData :m_KnownHouses)
	{
		if(houseData->LootCD(currentRuntime)>OldestLootTimer->LootCD(currentRuntime))
			OldestLootTimer=houseData;

		if(houseData->LootCD(currentRuntime)> lootTimerTreshold)
		{
			if(ClosestHouse==nullptr)
				ClosestHouse=houseData;
			else if(houseData->DistanceTo(targetPoint)<ClosestHouse->DistanceTo(targetPoint))
				ClosestHouse = houseData;
		}
	}

	if(ClosestHouse!=nullptr)
		return ClosestHouse;
	return OldestLootTimer;

}


HouseData* BotMemory::GetHouseData(HouseInfo newHouse)
{
	if(m_KnownHouses.size()<=0)
		return nullptr;

	const auto it = std::find_if(m_KnownHouses.begin(),m_KnownHouses.end(),[newHouse](HouseData* knownHouse){ return (knownHouse->baseInfo.Center==newHouse.Center); });
	if(it!=m_KnownHouses.end())
		return *it;
	else
		return nullptr;
}

bool BotMemory::AddHouse(HouseInfo newHouse)
{
	if(GetHouseData(newHouse)==nullptr)
	{
		m_NewHouses++;
		m_KnownHouses.push_back(new HouseData(newHouse,m_pNavGrid));
		return true;
	}
	return false;
}

bool BotMemory::SeenItem(EntityInfo newItem)
{
	if(m_SeenItems.size()>0)
	{	
		const auto it = std::find_if(m_SeenItems.begin(), m_SeenItems.end(), [newItem](EntityInfo seenItem) { return (seenItem.Location==newItem.Location); });
		if (it != m_SeenItems.end())
			return true;
	}
	return false;
}

bool BotMemory::AddItem(EntityInfo newItem)
{
	if(!SeenItem(newItem))
	{
		m_SeenItems.push_back(newItem);	
		return true;
	}
	return false;
}

void BotMemory::RemoveItem(EntityInfo newItem)
{
	//Removes all item at this location, but even if they spawn on top of each other, they will be picked up next fame regardless
	if(m_SeenItems.size()>0)
		m_SeenItems.erase(std::remove_if(m_SeenItems.begin(), m_SeenItems.end(), [newItem](EntityInfo listItem){ return listItem.Location==newItem.Location; }), m_SeenItems.end());
}

vector<EnemyData> BotMemory::GetKillableEnemies()
{
	if(m_VisibleEnemies.size()==0)
		return {};

	vector<EnemyData> returnVec = {};
	for(auto eData:m_VisibleEnemies)
	{
		if(eData.m_CanKill)
			returnVec.push_back(eData);
	}
	return returnVec;
}
