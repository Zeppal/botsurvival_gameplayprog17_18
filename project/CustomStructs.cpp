#include "stdafx.h"
#include "CustomStructs.h"
#include "ExplorationGrid.h"


/******************/
//HouseData

HouseData::HouseData(HouseInfo info, ExplorationGrid* navGrid):m_LastLootTime(0),baseInfo(info)
{
		m_vHouseCells = navGrid->GetHouseCells(info);
}

//HouseData::~HouseData()
//{
//	for(auto cell: m_vHouseCells)
//	{
//		SAFE_DELETE(cell);
//	}
//}


vector<Elite::Vector2> HouseData::GetLootPoints()
{
	vector<Elite::Vector2> lootSpots={baseInfo.Center};

	for(auto cell : m_vHouseCells)
	{
		lootSpots.push_back(cell->center);
	}
	return lootSpots;
}

float HouseData::DistanceTo(Elite::Vector2 target)
{
	return Elite::Distance(baseInfo.Center,target);
}

float HouseData::LootCD(float runTime)
{
	return (runTime- m_LastLootTime);
}
