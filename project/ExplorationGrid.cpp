#include "stdafx.h"
#include "ExplorationGrid.h"
#include <memory>
#include "CustomStructs.h"
#include "HelperFunctions.h"

ExplorationGrid::ExplorationGrid(float cellSize, Elite::Vector2 worldSize):m_CellSize(cellSize), m_WorldSize(worldSize)
{
	auto halfWorldSize=worldSize/2;

	for (float i = -halfWorldSize.x; i < halfWorldSize.x; i += cellSize)
	{

		for (float j = -halfWorldSize.y; j < halfWorldSize.y; j += cellSize)
		{
			auto newCell = new NavCell(m_vWorldCells.size(), cellSize,{i+cellSize/2,j+cellSize / 2 });
			m_vWorldCells.push_back(newCell);
			m_vUnExploredCells.push_back(newCell);
		}
	}
	auto a = 5;
}

ExplorationGrid::~ExplorationGrid()
{
	for(auto cell : m_vWorldCells)
	{
		SAFE_DELETE(cell);
	}
	m_LootPoints.clear();
}

void ExplorationGrid::Explore(Elite::Vector2 point,bool removeLootPoints)
{
	if(m_LootPoints.size()>0 || m_vUnExploredCells.size()>0)
	{
		auto exploredCell = getCell(point);
		if (removeLootPoints && m_LootPoints.size()>0)
		{
			m_LootPoints.erase(std::remove(m_LootPoints.begin(), m_LootPoints.end(), exploredCell->center), m_LootPoints.end());
		}

		if ( m_vUnExploredCells.size()>0)
		{
		if(exploredCell->explored)
			return;

		exploredCell->explored = true;
		m_vUnExploredCells.erase(std::remove(m_vUnExploredCells.begin(), m_vUnExploredCells.end(), exploredCell), m_vUnExploredCells.end());
		}
	}
}

void ExplorationGrid::ExploreArea(Elite::Vector2 center, Elite::Vector2 size)
{
	for (float xpos = center .x-size.x/2;  xpos < center.x + size.x / 2; xpos+= m_CellSize)
	{
		for (float ypos = center.y - size.y / 2; ypos < center.y + size.y / 2; ypos += m_CellSize)
		{
			Explore({xpos,ypos},false);
		}
	}
}

std::vector<NavCell*> ExplorationGrid::GetHouseCells(HouseInfo house)
{
	std::vector<NavCell*> returnVector;
	for (float xpos = house.Center.x - house.Size.x / 2; xpos < house.Center.x + house.Size.x / 2; xpos += m_CellSize)
	{
		for (float ypos = house.Center.y - house.Size.y / 2; ypos < house.Center.y + house.Size.y / 2; ypos += m_CellSize)
		{
			auto cell = getCell({ xpos,ypos });
			if(Helper::PointInHouse(house,cell->center))			
			{
				returnVector.push_back(cell);
			}
		}
	}
	return returnVector;
}

void ExplorationGrid::LootHouseCells(HouseInfo house)
{
	for (float xpos = house.Center.x - house.Size.x / 2; xpos < house.Center.x + house.Size.x / 2; xpos += m_CellSize)
	{
		for (float ypos = house.Center.y - house.Size.y / 2; ypos < house.Center.y + house.Size.y / 2; ypos += m_CellSize)
		{
			auto cell = getCell({ xpos,ypos });
			if (Helper::PointInHouse(house, cell->center))
			{
				m_LootPoints.push_back(cell->center);
			}
		}
	}
}

NavCell* ExplorationGrid::getCell(Elite::Vector2 point)
{
	auto adjustedPoint = point+(m_WorldSize/2);
	
	//Safeguard, mostly for click testing
	{
		if(adjustedPoint.x<0)
			adjustedPoint.x=0.f;
		if (adjustedPoint.x>m_WorldSize.x)
			adjustedPoint.x = m_WorldSize.x;


		if (adjustedPoint.y<0)
			adjustedPoint.y = 0.f;
		if (adjustedPoint.y>m_WorldSize.y)
			adjustedPoint.y = m_WorldSize.y;
	}

	const int yIndex = static_cast<int>( adjustedPoint.y/m_CellSize);
	const int xIndex = static_cast<int>(adjustedPoint.x / m_CellSize);
	const int cellsPerCollum = static_cast<int>(ceil(m_WorldSize.y/m_CellSize));

	const int index = yIndex+cellsPerCollum*xIndex;

	return m_vWorldCells[index];

}

NavCell* ExplorationGrid::getClosestUnkownCell(Elite::Vector2 point)
{
	if(m_vUnExploredCells.size()<=0)
		return nullptr;

	auto it = std::min_element(m_vUnExploredCells.begin(), m_vUnExploredCells.end(),
		[point](NavCell* a, NavCell*b) {return a->distance(point)<b->distance(point); });

	return *it;
}