#pragma once
#include "Exam_HelperStructs.h"
#include "CustomStructs.h"

class ExplorationGrid;

class BotMemory
{
public:
	BotMemory(ExplorationGrid* explorationGrid);
	~BotMemory();
	void NextFrame();

	HouseData* GetHouseFromPoint(Elite::Vector2 targetPoint);

	
	int m_NewHouses = 0;
	vector<HouseData*> GetUncheckedHouses();
	HouseData* GetClosestHouse(Elite::Vector2 targetPoint,float lootTimerTreshold, float currentRuntime);

	HouseData* GetHouseData(HouseInfo newHouse);
	bool AddHouse(HouseInfo newHouse);
	
	bool SeenItem(EntityInfo newItem);
	bool AddItem(EntityInfo newItem);
	void RemoveItem(EntityInfo newItem);


	vector<EnemyData> GetKillableEnemies();

	vector<HouseData*> m_KnownHouses;
	vector<EntityInfo> m_SeenItems;

	vector<EnemyData> m_VisibleEnemies;

	ExplorationGrid* m_pNavGrid;
};

