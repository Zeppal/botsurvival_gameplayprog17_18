#include "stdafx.h"
#include "Plugin.h"
#include "IExamInterface.h"

//Own classes
#include "HelperFunctions.h"
#include "InfoDrawManager.h"
#include "BotNavigation.h"
#include "InventoryManager.h"
#include "ExplorationGrid.h"
#include "BotMemory.h"
#include "UtilityManager.h"


//#define MOUSE
#define DEBUG_DRAW

void Plugin::Initialize(IBaseInterface* pInterface, PluginInfo& info)
{
	//Called only once, during initialization

	//Retrieving the interface
	//This interface gives you access to certain actions the AI_Framework can perform for you
	m_pInterface = static_cast<IExamInterface*>(pInterface);

	//Bit information about the plugin
	//Please fill this in!!
	info.BotName = "Noximilien";
	info.Student_FirstName = "Tom";
	info.Student_LastName = "Partoens";
	info.Student_Class = "2DAE5";
}

//Called only once, during initialization
void Plugin::InitGameDebugParams(GameDebugParams& params)
{
	params.AutoFollowCam = true; //Automatically follow the AI? (Default = true)
	params.RenderUI = false; //Render the IMGUI Panel? (Default = true)
	params.SpawnEnemies = true; //Do you want to spawn enemies? (Default = true)
	params.EnemyCount = 20; //How many enemies? (Default = 20)
	params.GodMode = false; //GodMode > You can't die, can be usefull to inspect certain behaviours (Default = false)
							//params.LevelFile = "LevelTwo.gppl";
	params.AutoGrabClosestItem = false; //A call to Item_Grab(...) returns the closest item that can be grabbed. (EntityInfo argument is ignored)
	params.OverrideDifficulty = false; //Override Difficulty?
	params.Difficulty = 1.f; //Difficulty Override: 0 > 1 (Overshoot is possible, >1)
}

//Only Active in DEBUG Mode
void Plugin::ProcessEvents(const SDL_Event& e)
{
#ifdef  MOUSE
	//Demo Event Code
	switch (e.type)
	{
	case SDL_MOUSEBUTTONUP:
	{
		if (e.button.button == SDL_BUTTON_LEFT)
		{
			int x, y;
			SDL_GetMouseState(&x, &y);
			const Elite::Vector2 pos = Elite::Vector2(static_cast<float>(x), static_cast<float>(y));
			/*m_Target = m_pInterface->Debug_ConvertScreenToWorld(pos);*/
			if (m_pNavGrid->m_LootPoints.size() == 0)
				m_pNavGrid->m_LootPoints.push_back(m_pNavGrid->getCell(m_pInterface->Debug_ConvertScreenToWorld(pos))->center);
			else
				m_pNavGrid->m_LootPoints[0] = (m_pNavGrid->getCell(m_pInterface->Debug_ConvertScreenToWorld(pos))->center);
		}
		break;
	}
	}
#endif
}

void Plugin::DllInit(){ m_RunTime = 0; }
void Plugin::DllShutdown()
{
	SAFE_DELETE(m_pInfo);
	SAFE_DELETE(m_pNavGrid);
	SAFE_DELETE(m_pNavigation);
	SAFE_DELETE(m_pInventoryManager);
	SAFE_DELETE(m_pBotMemory);
	SAFE_DELETE(m_pUtilityManager);
}

//This function calculates the new SteeringOutput, called once per frame
SteeringPlugin_Output Plugin::UpdateSteering(float dt)
{
	m_RunTime +=dt;
	
	if(!FirstPassSetup)
	{	
		FirstPassSetup=true;
		maxHealth = m_pInterface->Agent_GetInfo().Health;
		maxEnergy = m_pInterface->Agent_GetInfo().Energy;

		m_pNavigation = new BotNavigation(m_pInterface);
		m_pNavGrid = new ExplorationGrid(10,m_pInterface->World_GetInfo().Dimensions);
		m_pBotMemory = new BotMemory(m_pNavGrid);
		m_pInventoryManager = new InventoryManager(m_pInterface,m_pBotMemory);

		m_pUtilityManager = new UtilityManager(m_pInterface,m_pInventoryManager,
												m_pNavigation,m_pNavGrid,m_pBotMemory);
		#ifdef DEBUG_DRAW
		m_pInfo = new InfoDrawManager(m_pInterface, m_pNavGrid,m_pBotMemory);
		#endif
	}

	//Tell some parts that we're on a new frame
	m_pInventoryManager->NewFrame();
	m_pNavigation->NewFrame(dt);
	m_pBotMemory->m_VisibleEnemies.clear();

	//Base variables
	auto steering = SteeringPlugin_Output();
	const auto agentInfo = m_pInterface->Agent_GetInfo();
	auto checkpointLocation = m_pInterface->World_GetCheckpointLocation();

	//Vision
	{
		//Look for houses
		auto vHousesInFOV = Helper::GetHousesInFOV(m_pInterface);	//uses m_pInterface->Fov_GetHouseByIndex(...)
		for(auto house:vHousesInFOV)
		{
			m_pNavGrid->ExploreArea(house.Center,house.Size);
			m_pBotMemory->AddHouse(house);
		}
		
		//Look for enemy / items
		EntityInfo EntityFOV={};
		for (int i = 0;; ++i)
		{
			if(m_pInterface->Fov_GetEntityByIndex(i,EntityFOV))
			{
				if (EntityFOV.Type == eEntityType::ENEMY)
				{
					//Gather Enemy Info
					EnemyInfo eInfo = {};
					m_pInterface->Enemy_GetInfo(EntityFOV, eInfo);
					m_pBotMemory->m_VisibleEnemies.push_back(
							EnemyData{
									eInfo,
									m_pInventoryManager->HasEnoughDamageToKill(eInfo.Health) 
								} );
					//Draw Healthbars
					#ifdef DEBUG_DRAW
						m_pInfo->m_EnemiesToDraw.push_back(eInfo);
					#endif
				}

				else if (EntityFOV.Type == eEntityType::ITEM)
				{
					m_pBotMemory->AddItem(EntityFOV);
					m_pInventoryManager->FoundItem(EntityFOV);
				}

				continue;
			}
			//No more Entitys
			break;				
		}											 
	}//End Of vision block


	//Get steering based on current state
	m_pUtilityManager->UpdateUtility(dt,steering);

	//*******************
	//End of update actions
	//Try to heal up with items in your inventory
	m_pInventoryManager->TookDamage(maxHealth - agentInfo.Health);
	m_pInventoryManager->LostEnergy(maxEnergy - agentInfo.Energy);
	
	//Tell the exploration grid where we are
	m_pNavGrid->Explore(agentInfo.Position);

	return steering;
}


//This function should only be used for rendering debug elements
void Plugin::Render(float dt) const
{
	#ifdef DEBUG_DRAW
	m_pInfo->Draw(m_pInterface->Agent_GetInfo(),m_Target,m_RunTime);
	#endif
}



