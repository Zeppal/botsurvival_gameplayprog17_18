#pragma once
#include "Exam_HelperStructs.h"
#include "ExplorationGrid.h"

class IExamInterface;
class ExplorationGrid;
class BotMemory;

class InfoDrawManager
{
public:
	InfoDrawManager(IExamInterface* Interface, ExplorationGrid* NavGrid, BotMemory* Memory);
	~InfoDrawManager();

	void Draw(AgentInfo Agent,Elite::Vector2 Target, float totalRuntime);
	void DrawCell(NavCell* cell, Elite::Vector3 color = { 1,1,1 });
	void DrawNumber(Elite::Vector2 origin, int Number, Elite::Vector3 color={1,1,1});

	IExamInterface* m_pInterface;
	ExplorationGrid* m_pNavGrid;
	BotMemory* m_pBotMemory;

	std::vector<EnemyInfo>m_EnemiesToDraw;

	bool hasWorldInfo=false;
	Elite::Vector2 WorldDimensions; //Halved for easyer math


	int tileWidth = 10;
	int tileHeight = 10;
};

