#include "stdafx.h"
#include "BotNavigation.h"
#include "IExamInterface.h"
#include "HelperFunctions.h"
#include "CustomStructs.h"

BotNavigation::BotNavigation(IExamInterface* Interface):m_pInterface(Interface),m_TargetAngle(0)
{
}

BotNavigation::~BotNavigation()
{
}

void BotNavigation::NewFrame(float dt)
{
	(void) dt;
	m_LookingAtTarget=false;
}

void BotNavigation::UpdateAngularVelocity(SteeringPlugin_Output& steering)
{	
	if(m_LookingAtTarget)
		return;

	steering.AutoOrientate=true;
	
}

void BotNavigation::SetRunMode(SteeringPlugin_Output& steering, AgentInfo agent, bool hasPanic)
{
	//slowly aproach enemys, easyer aim, and they could pass you by
	if(m_LookingAtTarget)
	{
		m_StaminaRun=false;
		steering.RunMode = false;
	}

	//Run until out of stamina when in a panic
	if(m_PanicRecovery==false && hasPanic)
	{
		if(agent.Stamina > 0)
		{
			steering.RunMode = true;
			return;			
		}
		else
			m_PanicRecovery = true;
	}
	if(m_PanicRecovery &&agent.Stamina >= STAMINATRESHOLD)
		 m_PanicRecovery=false;


	//When not in a panic, maximize stamina use
	if(agent.Stamina>=10.f) //Max stamina acording to plugin, but no variable to check value
	{
		m_StaminaRun=true;
	}else if(agent.Stamina<= STAMINATRESHOLD)
	{
		m_StaminaRun =false;
	}
	steering.RunMode=m_StaminaRun;
}


void BotNavigation::LookAt(SteeringPlugin_Output& steering, AgentInfo agent, Elite::Vector2 target)
{

	auto agentAngle = fmod(agent.Orientation,3.15f);
	auto AngleToEnemy = Helper::getEnemyAngle(agent.Position, target);
	const float angleDiff = agentAngle - Helper::getEnemyAngle(agent.Position, target);
	
	//If this is the first target, set current angle difference
	if(!m_LookingAtTarget)
		m_TargetAngle=angleDiff;
	
	//If we are looking at something, and the new target angle is bigger, keep last target
	else if(abs(m_TargetAngle)< abs(angleDiff))
		return;
		
	steering.AngularVelocity= -10 * angleDiff;
	//std::cout<<"agent: "<< agentAngle <<"   ||||   toEnemy"<<AngleToEnemy << "   ||||   AngleDiff" <<angleDiff<<std::endl;
	m_LookingAtTarget=true;
	steering.AutoOrientate = false;
}
