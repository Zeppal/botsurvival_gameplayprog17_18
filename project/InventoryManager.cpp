#include "stdafx.h"
#include "InventoryManager.h"
#include "InfoDrawManager.h"
#include "Exam_HelperStructs.h"
#include <ppltasks.h>
#include "IExamInterface.h"
#include "BotMemory.h"


InventoryManager::InventoryManager(IExamInterface* Interface, BotMemory* Memory) :m_pInterface(Interface),m_pBotMemory(Memory)
{
}


InventoryManager::~InventoryManager()
{
}

void InventoryManager::NewFrame()
{
	m_CanUseThisFrame=true;
}


//#define  DEBUG_COUT

void InventoryManager::FoundItem(EntityInfo Item)
{
	auto agent = m_pInterface->Agent_GetInfo();
	if (Item.Type != eEntityType::ITEM ||  Elite::Distance(agent.Position, Item.Location)>agent.GrabRange)
		return;


	//Gather Item Info
	ItemInfo iInfo = {};
	if(m_pInterface->Item_Grab(Item, iInfo)==false)
	{
	#ifdef DEBUG_COUT
		std::cout << std::endl << "!+++!" << std::endl
			<< "distance: " << Elite::Distance(agent.Position, Item.Location) << " / " << agent.GrabRange << std::endl
			<< "ItemType: " << static_cast<int>(iInfo.Type) << std::endl
			<< "  : Slots: " << m_slotsFilled[0] << ", " << m_slotsFilled[1] << ", " << m_slotsFilled[2] << ", " << m_slotsFilled[3] << ", " << m_slotsFilled[4] << std::endl
			<< "!+++!" << std::endl;
	#endif
		return;
	};

	#ifdef DEBUG_COUT
		std::cout<<std::endl<<"---" << std::endl
		<<"distance: "<< Elite::Distance(agent.Position, Item.Location) <<" / "<<agent.GrabRange<< std::endl
		<< "ItemType: "<<static_cast<int>(iInfo.Type) << std::endl 
		<<"  : Slots: "<<m_slotsFilled[0] << ", "<<m_slotsFilled[1]<< ", " << m_slotsFilled[2]<< ", " << m_slotsFilled[3]<< ", " << m_slotsFilled[4] <<std::endl << "---"<<std::endl;
	#endif

	m_pBotMemory->RemoveItem(Item);	

	if (iInfo.Type == eItemType::PISTOL) 
	{
		FoundGun(iInfo);
	}

	else if (iInfo.Type == eItemType::FOOD)
	{
		FoundFood(iInfo);
	}
	else if (iInfo.Type == eItemType::MEDKIT)
	{
		FoundMedKit(iInfo);
	}
	else if (iInfo.Type == eItemType::GARBAGE)
	{
		FillSlot(4, iInfo);
	}
	else
	{
		return;
	}

	//Always destroy or consume the item in the last slot
	if(m_slotsFilled[4])
	{
		ItemInfo tempSlotItem = {};
		m_pInterface->Inventory_GetItem(4, tempSlotItem);
		if(tempSlotItem.Type==eItemType::GARBAGE || tempSlotItem.Type == eItemType::PISTOL)
		{
			RemoveSlot(4);
		}
		else
			Consume(4);
	};
	
	return;
}

void InventoryManager::Consume(UINT slot)
{
	if(m_CanUseThisFrame)
		m_pInterface->Inventory_UseItem(slot);
	m_CanUseThisFrame=false;

	RemoveSlot(slot);
}

void InventoryManager::FillSlot(UINT slot,ItemInfo item)
{
	if(m_slotsFilled[slot])
		RemoveSlot(slot);

	m_pInterface->Inventory_AddItem(slot, item);
	m_slotsFilled[slot] = true;
}

void InventoryManager::RemoveSlot(UINT slot)
{
	if(m_slotsFilled[slot])
		m_pInterface->Inventory_RemoveItem(slot);

	m_slotsFilled[slot] = false;
}

struct Gun
{
	int ammo;
	float dps;
	float range;
	Gun(IExamInterface* Interface, ItemInfo e)
	{
		ammo = Interface->Item_GetMetadata(e, "ammo"); //INT
		dps = Interface->Item_GetMetadata(e, "dps"); //FLOAT
		range = Interface->Item_GetMetadata(e, "range"); //FLOAT
	}
	float TotalDamage() { return dps*ammo; }
};

//Always keep the two guns with the most total damage left
void InventoryManager::FoundGun(ItemInfo FoundItem)
{
	ItemInfo slot0={};
	ItemInfo slot1 = {};

	if (!m_slotsFilled[0])
	{	
		FillSlot(0, FoundItem);
		return;
	}
	if (!m_slotsFilled[1])
	{
		FillSlot(1, FoundItem);
		return;
	}


	m_pInterface->Inventory_GetItem(0,slot0);
	m_pInterface->Inventory_GetItem(1, slot1);

	//Both slots already have a gun
	Gun gun0 = Gun(m_pInterface,slot0);
	Gun gun1 = Gun(m_pInterface, slot1);
	Gun newGun = Gun(m_pInterface,FoundItem);

	if(newGun.TotalDamage()>gun0.TotalDamage())
	{
		RemoveSlot(0);
		FillSlot(0, FoundItem);
		return;
	}
	if (newGun.TotalDamage()>gun1.TotalDamage())
	{
		RemoveSlot(1);
		FillSlot(1, FoundItem);
		return;
	}
	FillSlot(4, FoundItem); //Will be removed
}


//is kept in slot 3 but food takes priority
void InventoryManager::FoundMedKit(ItemInfo FoundItem)
{
	ItemInfo slot3 = {};
	if (!m_slotsFilled[3]) //If we don't have anything in 3 anyway, keep the medkit
	{
		FillSlot(3, FoundItem);
		return;
	}

	m_pInterface->Inventory_GetItem(3, slot3);
	if(slot3.Type==eItemType::FOOD) //Food is more valueble, so consume the healthpack to heal any possible lost health
	{
		FillSlot(4, FoundItem);		
		return;
	}
	//Slot 3 will only hold food or other medpacks
	int FoundHealth = m_pInterface->Item_GetMetadata(FoundItem, "health");
	int SlotHealth = m_pInterface->Item_GetMetadata(slot3, "health"); 

	if(FoundHealth>SlotHealth)
	{
		Consume(3);
		FillSlot(3, FoundItem);
		return;
	}
	FillSlot(4, FoundItem); //Use MedKit
}

void InventoryManager::FoundFood(ItemInfo FoundItem)
{

	if (!m_slotsFilled[2])
	{
		FillSlot(2, FoundItem);
		return;
	}

	if (!m_slotsFilled[3])
	{
		FillSlot(3, FoundItem);
		return;
	}

	ItemInfo slot2 = {};
	ItemInfo slot3 = {};
	m_pInterface->Inventory_GetItem(2, slot2);
	m_pInterface->Inventory_GetItem(3, slot3);


	if(slot3.Type==eItemType::MEDKIT)
	{
		//Consume(3);
		RemoveSlot(3);
		FillSlot(3, FoundItem);
		return;
	}

	//If it isn't empty or an medkit, it's food.
	// Eat the lowest energy food you are holding
	int FoundEnergy = m_pInterface->Item_GetMetadata(FoundItem, "energy");
	int Slot2Energy = m_pInterface->Item_GetMetadata(slot2, "energy");
	if(FoundEnergy>Slot2Energy)
	{
		Consume(2);
		FillSlot(2, FoundItem);
		return;
	}

	int Slot3Energy = m_pInterface->Item_GetMetadata(slot2, "energy");
	if (FoundEnergy>Slot3Energy)
	{
		Consume(3);
		FillSlot(3, FoundItem);
		return;
	}
		
	//Picked up the lowest energy food, eat it anyway
	FillSlot(4, FoundItem);
}

bool InventoryManager::HasEnoughDamageToKill(int enemyHp)
{
	float totalDamage=0;

	if(m_slotsFilled[0])
	{
		ItemInfo slot0 = {};
		m_pInterface->Inventory_GetItem(0, slot0);
		auto gun0 = Gun(m_pInterface,slot0);
		totalDamage+=gun0.TotalDamage();
	}

	if (m_slotsFilled[1])
	{
		ItemInfo slot1 = {};
		m_pInterface->Inventory_GetItem(1, slot1);
		auto gun1 = Gun(m_pInterface, slot1);
		totalDamage += gun1.TotalDamage();
	}
	return (totalDamage>=enemyHp);
}

bool InventoryManager::ShootEnemy(EnemyInfo Enemy)
{
	bool KilledEnemy = false;
	bool hasShot = false;

	if(!m_CanUseThisFrame)
		return false;

	auto threatRange = Distance(Enemy.Location,m_pInterface->Agent_GetInfo().Position);
	int damageToDeal = Enemy.Health;

	if(m_slotsFilled[0]&& m_slotsFilled[1])
	{
		ItemInfo slot0={};
		m_pInterface->Inventory_GetItem(0, slot0);
		Gun gun0 = Gun(m_pInterface, slot0);
		
		ItemInfo slot1 = {};
		m_pInterface->Inventory_GetItem(1, slot1);
		Gun gun1 = Gun(m_pInterface, slot1);


		//Shoot the gun with the lowest damage totally, will empty out weak guns first
		if(gun1.TotalDamage()<=gun0.TotalDamage()  && gun1.range>=threatRange )
		{
			if(gun1.dps>=damageToDeal)
				KilledEnemy=true;

			m_pInterface->Inventory_UseItem(1);
			m_CanUseThisFrame=false;
			hasShot=true;
			if(gun1.ammo<=1)
			{
				RemoveSlot(1);
			}
		}
		else if(gun0.range >= threatRange) //Gun0 damage < gun1 damage
		{
			if (gun0.dps >= damageToDeal)
				KilledEnemy = true;

			m_pInterface->Inventory_UseItem(0);
			m_CanUseThisFrame = false;
			hasShot = true;
			if (gun0.ammo <= 1)
			{
				RemoveSlot(0);
			}	
		}
	}

	if (!hasShot&& m_slotsFilled[1])
	{
		ItemInfo slot1 = {};
		m_pInterface->Inventory_GetItem(1, slot1);
		Gun gun1 = Gun(m_pInterface, slot1);

		if(gun1.range>=threatRange)
		{ 
			if (gun1.dps >= damageToDeal)
				KilledEnemy = true;

			m_pInterface->Inventory_UseItem(1);
			m_CanUseThisFrame = false;
			hasShot = true;
			if (gun1.ammo <= 1)
			{
				RemoveSlot(1);
			}
		}
	}

	if (!hasShot&& m_slotsFilled[0])
	{
		ItemInfo slot0 = {};
		m_pInterface->Inventory_GetItem(0, slot0);
		Gun gun0 = Gun(m_pInterface, slot0);

		if(gun0.range>=threatRange)
		{
			if (gun0.dps >= damageToDeal)
				KilledEnemy = true;

			m_pInterface->Inventory_UseItem(0);
			m_CanUseThisFrame = false;
			hasShot = true;
			if (gun0.ammo <= 1)
			{
				RemoveSlot(0);
			}
		}
	}
	
	return KilledEnemy;
}

void InventoryManager::TookDamage(float damage)
{
	if(m_CanUseThisFrame && m_slotsFilled[3])//Don't waste healing because you picked something up
	{
		ItemInfo slot3 = {};
		m_pInterface->Inventory_GetItem(3, slot3);

		if (slot3.Type==eItemType::MEDKIT)
		{
			int medKitHealing = m_pInterface->Item_GetMetadata(slot3, "health");
			if( damage >= static_cast<float>(medKitHealing))
			{
				Consume(3);
			}
		}
	}



}

void InventoryManager::LostEnergy(float energyLoss)
{
	if (m_CanUseThisFrame && (m_slotsFilled[2] || m_slotsFilled[3]))//Don't waste healing because you picked something up
	{
		//Try slot 3 first, if it's apropriate, it will open op a slot for a medkit
		if(m_slotsFilled[3])
		{
			ItemInfo slot3 = {};
			m_pInterface->Inventory_GetItem(3, slot3);
			if (slot3.Type == eItemType::FOOD)
			{
				int foodEnergy = m_pInterface->Item_GetMetadata(slot3, "energy");
				if (energyLoss >= static_cast<float>(foodEnergy))
				{
					Consume(3);
					return;
				}
			}
		}

		// slot2 only holds food
		if (m_slotsFilled[2]) {
			ItemInfo slot2 = {};
			m_pInterface->Inventory_GetItem(2, slot2);
			int foodEnergy = m_pInterface->Item_GetMetadata(slot2, "energy");
			if (energyLoss >= static_cast<float>(foodEnergy))
			{
				Consume(2);
				return;
			}
		}
	}
}


//Min- 0
//Max- 5
int InventoryManager::GetEmptySlots()
{	
	float emptySlots=0;
	//When both guns are empty, count it as 3 slots (important for utility level)
	if(!m_slotsFilled[0])
		emptySlots+=1.5f;
	if (!m_slotsFilled[1])
		emptySlots += 1.5f;

	if (!m_slotsFilled[2])
		emptySlots += 1.5f; //Will be consumed last => most important of the two utility slots
	if (!m_slotsFilled[3])
		emptySlots += 1;
	 
	return static_cast<int>(floor(emptySlots));
}



