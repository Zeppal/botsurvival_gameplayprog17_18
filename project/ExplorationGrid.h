#pragma once
#include <Exam_HelperStructs.h>
#include "CustomStructs.h"


class ExplorationGrid
{
public:
	ExplorationGrid(float cellSize,Elite::Vector2 worldSize);
	~ExplorationGrid();
	void Explore(Elite::Vector2 point, bool removeLootPoints=true);
	void ExploreArea(Elite::Vector2 center, Elite::Vector2 size);
	std::vector<NavCell*> GetHouseCells(HouseInfo house);
	void LootHouseCells(HouseInfo house);


	NavCell* getCell(Elite::Vector2 point);
	NavCell* getClosestUnkownCell(Elite::Vector2 point);

	int UnexploredCells() { return m_vUnExploredCells.size(); }


//protected:
	const float m_CellSize;
	Elite::Vector2 m_WorldSize;
	std::vector<NavCell*> m_vWorldCells;
	std::vector<NavCell*> m_vUnExploredCells;

	//Saved in exploration grid, since it will always be checking the players location for cell exploration
	std::vector<Elite::Vector2> m_LootPoints;
};

