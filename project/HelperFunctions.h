#pragma once
#include <vector>
#include "Exam_HelperStructs.h"
class IExamInterface;

class Helper
{
public:
	static Elite::Vector2 getRotationToPoint(const float rotation, Elite::Vector2 startRotation = Elite::Vector2{ 0,-1 });
	static float getEnemyAngle(Elite::Vector2 agentPos, Elite::Vector2 enemyPos, const float baseRotation = Elite::ToRadians(90));
	static vector<HouseInfo> GetHousesInFOV(IExamInterface* Interface);
	static vector<EntityInfo> GetEntitiesInFOV(IExamInterface* Interface);
	static bool PointInHouse(HouseInfo house, Elite::Vector2 point);
};
