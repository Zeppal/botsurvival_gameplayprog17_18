#pragma once
#include "IExamPlugin.h"
#include <Exam_HelperStructs.h>

class InfoDrawManager;
class IBaseInterface;
class IExamInterface;
class InventoryManager;
class BotNavigation;
class ExplorationGrid;
class BotMemory;
class UtilityManager;


class Plugin :public IExamPlugin
{
public:
	Plugin() {};
	virtual ~Plugin() {};

	void Initialize(IBaseInterface* pInterface, PluginInfo& info) override;
	void DllInit() override;
	void DllShutdown() override;

	void InitGameDebugParams(GameDebugParams& params) override;
	void ProcessEvents(const SDL_Event& e) override;

	SteeringPlugin_Output UpdateSteering(float dt) override;
	void Render(float dt) const override;


private:
	//Interface, used to request data from/perform actions with the AI Framework
	IExamInterface* m_pInterface = nullptr;

	Elite::Vector2 m_Target = {};
	bool m_CanRun = false; //Demo purpose
	float m_AngSpeed = 0.f; //Demo purpose



	bool FirstPassSetup=false;
	InfoDrawManager* m_pInfo;
	InventoryManager* m_pInventoryManager;
	BotNavigation* m_pNavigation;
	ExplorationGrid* m_pNavGrid;
	BotMemory* m_pBotMemory;

	float maxEnergy;
	float maxHealth;
	float m_RunTime=0;
	UtilityManager* m_pUtilityManager;
};

//ENTRY
//This is the first function that is called by the host program
//The plugin returned by this function is also the plugin used by the host program
extern "C"
{
	__declspec (dllexport) IPluginBase* Register()
	{
		return new Plugin();
	}
}